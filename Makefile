SRC_MD = $(shell ls -1 *.md)
OUT_HTML = $(SRC_MD:.md=.html)
 
default: all

all: $(OUT_HTML)

%.html: %.md
	markdown $< > $@

clean:
	$(RM) -d $(OUT_HTML)

.PHONY: default all clean
