<style>
img {
	height: 50%;
}
</style>

# Reverse Engineering the SumUp System Architecture

## Motivation

As preparation for my job interview at SumUp I decided to show what I would be able to figure out about their product and system architecture only using public available sources.

The vacancy for "Embedded Software Engineer" that lead to my application and further communication can be found [here](https://sumup.workable.com/j/4EB89C52FC).

## Starting Point

I started with what I could find on the website of SumUp ...

The product provided by SumUp is called the SumUp Air. It is a mobile "Point-of-Sale" (mPOS) device that allows performing financial transactions using payment cards in combination with a Bluetooh capable smart phone or table computer.

In addition, SumUp also offers something called the SumUp POS Cloud at https://pos.sumup.com.

### Features

 * smartCard reader
 * magnetic card reader
 * NFC (card or smart phone) reader
 * Bluetooth interface towards smart phone of merchant
 * re-charagle battery powered, chargable via microUSB
 * OLED display
 * number keypad

Note that the US website shows a device that has no display or keypad but looks very similar to what I have seen to be the SumUp Air.

### Use Cases

The inital steps look like this:

 * buy a SumUp Air
 * install the merchant application (app) on a smart phone
 * create a user account from insdide the app

The repeating per-customer steps look like this:

 * merchant opens the app on the smart phone
 * a amount to be wired is entered
 * the SumUp Air magically wakes up (and might show the amount to be withdrawn)
 * customer "connects" payment card in some way to the SumUp Air device
 * SumUp Air performs read of the payment card and sends it back to the smart phone
 * the app on the smart phone talks to the backend servers of SumUp to complete the transaction

## Software

After having peek at the [SumUp GitHub account](https://github.com/sumup) and using some standard networking tools (host, ping, nmap) I could infer various software aspects.

Website

  * sumup.com is load balanced by Amazon CloudFront
  * Contentful CMS is used
  * uses SASS (brand-styles repo)
  * uses Go and Docker (emporium repo)

Backend

  * Terrafrom is used to describe/manage architecture(?)
  * Business Logic
   * core payment system implemented in Elixir Erlang
    * Yaws HTTPD library is used
    * SOAP interface might be used
    * JSON for RPC is use
    * gettext might be used(?)
    * Mustache template engine
    * SMTP is used for sending E-Mails
    * SMPP implementation in Ruby might be used to send SMS
  * Persistence
   * PostgreSQL is used at some point
   * Riak (NoSQL) key-value data store is used
   * RabbitMQ used in combination with Riak
  * Google Protobuf is used in some combination with Go

Apps

  * implemented as native (got that from some post about SumUp mobile meetup)
  * for earlier hardware the audio jack was used to connect to the mPOS (TheAmazingAudioEngine repo and old pictures allow that assumption)
  * iOS
   * Swift is used
   * Keychain integration

Hardware

 * Linux is used at some point
 * u-boot boot loader
 * Yocto is used (something between guessing and hoping)
 * Qt is used on some devices (I haven't seen yet - maybe the new hardware)

And the bits about software that is used internally ...

 * Atlassian (Jira?) https://sumupteam.atlassian.net

### Speculative System Architecture

After having a rough idea what the product of SumUp is, what it does and the various software components used, I came up with a (in part speculative) list of components that must be part of the system architecture.

 * SumUp Air (mPOS) hardware
 * smart phone applications
 * servers that are visible on the internet
  * [Company Website](https://sumup.com) implemented using Contentful CMS
  * [Static Content Websever](https://static.sumup.com)
  * [Shop](https://shop.sumup.com) implemented using Shoppify
  * [SumUp Dashboard](https://me.sumup.com)
  * [SumUp POS Cloud](https://pos.sumup.com)
  * [SumUp Support Center](https://help.sumup.com)
 * servers that must be part of the backend
  * user account management
  * payment gateways to banks as part of the backend
  * monitoring of transactions & user accounts
  * monitoring of devices
  * mPOS firmware update management

<pre>
                                                      +---------+
+--------+            +----------+                    |         |
|        | Bluetooth  |          |                  +---------+ |
|  mPOS  | . . . . .  |  Smart   |   Internet via   |         | |
|        |            |  Phone   |   GSM or WiFi  +---------+ | |
+--------+            |    or    | . . . . . . .  |         | | |
                      | Tablet   |                |         | | |
                      | Computer |                | Servers | |-+
                      |          |                |         | |
                      +----------+                |         |-+
                                                  |         |
                                                  +---------+
</pre>



## Hardware

Using Google Image Search I managed to get hold of two photos of the SumUp Air PCB which provided me with more insight into the hardware components actually used on the mPOS.

![SumUp Air PCB smartcard reader side](images/sumup-air-pcb-1.jpg)

![SumUp Air PCB magnetic card reader side](images/sumup-air-pcb-2.jpg)

### Integrated Circuits

5 big ICs on smartCard reader side

<pre>
| Vendor      | Chip                    | Function                  |
| ----------- | ----------------------- | ------------------------- |
| Maxim       | USIP PRO IC0400C-778-BF | MIPS32 4KSd SoC with MMU  |
| NXP         | TDA8035                 | Smart Card Interface      |
| Magtek      | 21006541                | Triple Track F/2F Decoder |
| Microchip   | QT60160                 | Key QMatrix Touch Sensor  |
| Microchip   | 38VF6401                | Flash                     |
</pre>

1 big IC on the magnetic card reader side

<pre>
| Vendor | Chip     | Function       |
| ------ | -------- | -------------- |
| Nordic | nRF52832 | Bluetooth/NFC  |
</pre>

#### Datasheets

 * [Maxim USIP PRO](documents/datasheets/Maxim_IC0400C778BF-USIP.pdf)
 * [Nordic nRF52832](documents/datasheets/Nordic_nRF52832.pdf)
 * [NXP TDA8035](documents/datasheets/NXP_TDA8035.pdf)
 * [Magtek 21006541](documents/datasheets/Magtek_21006541.pdf)
 * [Microchip QT60160](documents/datasheets/Microchip_QT60240.pdf)
 * [Microchip 38VF6401](documents/datasheets/Microchip_38VF6401.pdf)

### Block Diagram


<pre>
+-----------------+                    +---------------------+
|    SmartCard    |                    |     Flash Memory    |
|    Interface    |                    |                     |
+-----------------+                    +---------------------+
         |              serial I/O                |
         +------------------+                     | parallel I/O interface (21-bit address, 16-bit data)
                            |                     |
                            |          +---------------------+
+-----------------+         +----------|                     |                                  +------+
|  Magnetic Card  |     serial I/O     |  Secure Application |------------------+---------------| µUSB |
|    Interface    |--------------------|      Processor      |                  |               +------+
+-----------------+         +----------|                     |                  |
                            |          +---------------------+                  |
                            |                     |                             |
         +------------------+                     | I2C               +--------------------+
         |              serial I/O (SPI or I2C)   |                   |                    |
+-----------------+                    +---------------------+        |  Power & Charging  |
|  Bluetooth/NFC  |                    |    Touch Matrix     |        |       Circuitry    |
|  Transceiver    |                    |     Controller      |        |                    |
+-----------------+                    +---------------------+        +--------------------+
</pre>


## Future Device

The description of the job vacancy lists "Knowledge of TI Sitara processors". As I uncovered, at least the hardware from the picture does not use a ARM but a MIPS processor. Hence I suspect a new hardware version is being developed.

I had a look at the TI Sitara product matrix, set "Serial I/O -> Smart Card" as a filter and ended up with three candidates.

 * AM4382
 * AM4383
 * AM4388

All of them being ARM Cortex-A9 devices.

While the AM4382 being the only one that can operate at 300 MHz while all others rung at 600 MHz as their lowest speed. Lowest speed and hance reduced power consumption is one of the key aspects of the device. So I guess the next version of the hardware will use a AM4382.

Further more the devices above the AM4382 have a 3D unit and PRU-ICs (additional micro-controllers on the SoC) which I suspect are not needed for the new hardware.

Only the "preview" status of the AM4382 makes it less likely that this model is used but the "active" AM4388.
